package mx.com.lickodev.udemy.control.users.repository;

import mx.com.lickodev.udemy.control.commons.constants.ErrorMessage;
import mx.com.lickodev.udemy.control.commons.entity.projections.IUserDTO;
import mx.com.lickodev.udemy.control.commons.entity.projections.IUserProjectionCompleteDTO;
import mx.com.lickodev.udemy.control.commons.entity.users.User;
import mx.com.lickodev.udemy.control.commons.exceptions.UserNotFoundException;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Optional;

import static mx.com.lickodev.udemy.control.commons.constants.ParameterNames.*;
import static mx.com.lickodev.udemy.control.commons.constants.ParameterSizes.MAX_LENGTH_ROLE_NAME;
import static mx.com.lickodev.udemy.control.commons.constants.ParameterSizes.MAX_LENGTH_USER_NAME;
import static mx.com.lickodev.udemy.control.commons.constants.Paths.*;

/**
 * @author saul_
 * <p>
 * <p>
 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections.interfaces
 * https://www.baeldung.com/spring-data-rest-projections-excerpts
 * <p>
 * Recordar que setear excerptProjection sólo servirá para listas en general (paginas también)
 * para elementos unicos como findByUserName o findById aplicará lo que se haya definido como retorno,
 * es decir, si se define IUserProjectionCompleteDTO eso devolverá, si se define User eso devolverá
 * y no es posible cambiarlo por medio de el parametro "projection" que indica la documentación de spring.
 * <p>
 * Es preferible usar interfaces ya que con estas si es posible hacer proxy para obtener las entidades anidadas,
 * en cambio hacerlo con clases concretas no lo permite.
 */
@Validated
@RepositoryRestResource(path = USERS_BASE_PATH, excerptProjection = IUserDTO.class)
public interface UsersRepository extends JpaRepository<User, Long> {

    @RestResource(path = USERS_FIND_BY_USER_NAME_PATH)
    IUserProjectionCompleteDTO findByUserName(@Param(PARAMETER_USER_NAME)
                                              @NotEmpty(message = ErrorMessage.LABEL_ERROR_NAME_MIN_MAX)
                                              @Length(min = 2, max = MAX_LENGTH_USER_NAME,
                                                      message = ErrorMessage.LABEL_ERROR_NAME_MIN_MAX) String userName) throws UserNotFoundException;

    @RestResource(path = USERS_CONTAINING_USER_NAME_PATH)
    Page<User> findByUserNameContaining(@Param(PARAMETER_USER_NAME)
                                        @NotEmpty(message = ErrorMessage.LABEL_ERROR_NAME_MIN_MAX)
                                        @Length(min = 2, max = MAX_LENGTH_USER_NAME,
                                                message = ErrorMessage.LABEL_ERROR_NAME_MIN_MAX) String userName,
                                        Pageable pageable);

    @RestResource(path = USERS_CONTAINING_ROLE_NAME_PATH)
    Page<User> findByRolesRoleNameContainingIgnoreCase(@Param(PARAMETER_ROLE_NAME)
                                                       @NotEmpty
                                                       @Length(min = 2, max = MAX_LENGTH_ROLE_NAME,
                                                               message = ErrorMessage.LABEL_ERROR_ROLE_NAME_MIN_MAX) String roleName,
                                                       Pageable pageable);

    @Override
    @RestResource(path = USERS_FIND_BY_ID_PATH)
    Optional<User>findById(Long id);

    @Override
    @RestResource(path = USERS_SAVE_PATH)

    User save(@Validated User user);
}
